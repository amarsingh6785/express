import { Routes } from '@angular/router';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { UserProfileComponent } from '../components/user-profile/user-profile.component';
import { MapsComponent } from '../components/maps/maps.component';
import { NotificationsComponent } from '../components/notifications/notifications.component';
import { CompanyComponent } from 'app/components/company/company.component';
import { LoginComponent } from 'app/components/login/login.component';
import { BulkUploadingComponent } from 'app/components/bulk-uploading/bulk-uploading.component';
import { BulkOrderPickUpRequestsComponent } from 'app/components/bulk-order-pick-up-requests/bulk-order-pick-up-requests.component';
import { CreateOrderComponent } from 'app/components/create-order/create-order.component';
import { DistributorComponent } from 'app/components/distributor/distributor.component';
import { CustomerComponent } from 'app/components/customer/customer.component';
import { AssignDriverComponent } from '../components/assign-driver/assign-driver.component';
import { RepresentativesManagementComponent } from 'app/components/representatives-management/representatives-management.component';
import { DriveManageComponent } from '../components/drive-manage/drive-manage.component';
import { ManageOrderComponent } from '../components/manage-order/manage-order.component';
import { ZonesComponent } from 'app/components/zones/zones.component';
import { PriceListComponent } from 'app/components/price-list/price-list.component';
import { ServicesComponent } from 'app/components/services/services.component';
import { TrackOrderComponent } from './track-order/track-order.component';
import { ManageRFQComponent } from './manage-rfq/manage-rfq.component';

export const AdminLayoutRoutes: Routes = [

    { path: 'dashboard', component: DashboardComponent },
    { path: 'company', component: CompanyComponent },
    { path: 'login', component: LoginComponent },
    { path: 'bulkUploading', component: BulkUploadingComponent },
    { path: 'bulkOrder', component: BulkOrderPickUpRequestsComponent },
    { path: 'createOrder', component: CreateOrderComponent },
    { path: 'user-profile', component: UserProfileComponent },
    { path: 'maps', component: MapsComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: 'distributor', component: DistributorComponent },
    { path: 'customer', component: CustomerComponent },
    { path: 'assignDriver/:id', component: AssignDriverComponent },
    { path: 'ManageDriver', component: DriveManageComponent },
    { path: 'manageOrder', component: ManageOrderComponent },
    { path: 'representativeManagement', component: RepresentativesManagementComponent },
    { path: 'zones', component: ZonesComponent },
    { path: 'priceList', component: PriceListComponent },
    { path: 'services', component: ServicesComponent },
    { path: 'trackOrder/:id', component: TrackOrderComponent },
    { path: 'manageRFQ', component: ManageRFQComponent },

];
