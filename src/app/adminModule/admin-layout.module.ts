import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { UserProfileComponent } from '../components/user-profile/user-profile.component';
import { MapsComponent } from '../components/maps/maps.component';
import { NotificationsComponent } from '../components/notifications/notifications.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { CompanyComponent } from 'app/components/company/company.component';
import { BulkOrderPickUpRequestsComponent } from 'app/components/bulk-order-pick-up-requests/bulk-order-pick-up-requests.component';
import { BulkUploadingComponent } from 'app/components/bulk-uploading/bulk-uploading.component';
import { CreateOrderComponent } from 'app/components/create-order/create-order.component';
import { LoginComponent } from 'app/components/login/login.component';
import { DataTablesModule } from "angular-datatables";
import { AgmCoreModule } from '@agm/core';
import { DistributorComponent } from '../components/distributor/distributor.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import { CustomerComponent } from '../components/customer/customer.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { AssignDriverComponent } from '../components/assign-driver/assign-driver.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { RepresentativesManagementComponent } from '../components/representatives-management/representatives-management.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { ColorPickerModule } from 'ngx-color-picker';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { DriveManageComponent } from '../components/drive-manage/drive-manage.component';
import { ManageOrderComponent } from '../components/manage-order/manage-order.component';
import { ZonesComponent } from 'app/components/zones/zones.component';
import { ServicesComponent } from 'app/components/services/services.component';
import { PriceListComponent } from 'app/components/price-list/price-list.component';
import { TrackOrderComponent } from './track-order/track-order.component';
import { ManageRFQComponent } from './manage-rfq/manage-rfq.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    NgxIntlTelInputModule,
    DataTablesModule,
    MatNativeDateModule,
    NgxBarcodeModule,
    MatDatepickerModule,
    ColorPickerModule,
    Ng2TelInputModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    // AgmCoreModule.forRoot({
    //   apiKey: "AIzaSyCDVnGbGw5JXOPoNqNxQTkjZzUx6zfDMFQ"
    // }),

  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    MapsComponent,
    NotificationsComponent,
    CompanyComponent,
    LoginComponent,
    BulkUploadingComponent,
    BulkOrderPickUpRequestsComponent,
    CreateOrderComponent,
    DistributorComponent,
    CustomerComponent,
    AssignDriverComponent,
    RepresentativesManagementComponent,
    DriveManageComponent,
    ManageOrderComponent,
    ZonesComponent,
    ServicesComponent,
    PriceListComponent,
    TrackOrderComponent,
    ManageRFQComponent
  ]
})

export class AdminLayoutModule { }
declare module "@angular/core" {
  interface ModuleWithProviders<T = any> {
    ngModule: Type<T>;
    providers?: Provider[];
  }
}