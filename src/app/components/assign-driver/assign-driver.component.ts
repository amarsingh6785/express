import { AfterViewInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { MouseEvent } from '@agm/core';

import { Validators } from '@angular/forms';
import { OnDestroy } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-assign-driver',
  templateUrl: './assign-driver.component.html',
  styleUrls: ['./assign-driver.component.css']
})
export class AssignDriverComponent implements OnInit, AfterViewInit, OnDestroy {
  dtElement: DataTableDirective;

  dtOptions: any = {};
  idForDelete: any;
  dtTrigger: Subject<any> = new Subject();


  orderDetails = []
  orderForm;
  isEdit: boolean;
  selectedLocationId: any;
  addOrEdit;
  dataSource;
  customerForm;
  areaList = []
  totalOrder = 0
  governorateList = [
    'Capital',
    'Hawalli',
    'Mubarak Al-Kaber',
    'Ahmadi',
    'Farwaniya',
    'Jahra'
  ]
  areaListByGovernorate = [{
    gov: 'Capital', area: [
      "Abdulla Al-Salem",
      "Adailiya",
      "Bnaid Al-Qar",
      "Daʿiya",
      "Dasma",
      "Doha",
      "Doha Port",
      "Faiha",
      "Failaka",
      "Ghornata",
      "Jaber Al-Ahmad City",
      "Jibla",
      "Kaifan",
      "Khaldiya",
      "Mansūriya",
      "Mirgab",
      "Nahdha",
      "North West Sulaibikhat",
      "Nuzha",
      "Qadsiya",
      "Qurtuba",
      "Rawda",
      "Shamiya",
      "Sharq",
      "Shuwaikh",
      "Shuwaikh Industrial Area",
      "Shuwaikh Port",
      "Sulaibikhat",
      "Surra",
      "Umm an Namil Island",
      "Yarmouk",

    ]
  }
    ,
  {
    gov: 'Hawalli', area: [
      "Anjafa",
      "Bayān",
      "Bi'da",
      "Hawally",
      "Hittin",
      "Jabriya",
      "Maidan Hawalli",
      "Mishrif",
      "Mubarak Al-Jabir",
      "Nigra",
      'Rumaithiya',
      "Salam",
      "Salmiya",
      "Salwa",
      "Sha'ab",
      "Shuhada",
      "Siddiq",
      "South Surra",
      'Zahra'


    ]
  },
  {
    gov: 'Mubarak Al-Kaber', area: [
      " Abu Al Hasaniya",
      "Abu Futaira",
      "Adān",
      "Al Qurain",
      "Al-Qusour",
      "Fintās",
      "Funaitīs",
      "Misīla",
      "Mubarak Al-Kabeer",
      "Sabah Al-Salem",
      "Sabhān",
      "South Wista",
      "Wista",


    ]
  },
  {
    gov: 'Ahmadi', area: [
      "Abu Halifa",
      "Abdullah Port",
      "Ahmadi",
      "Ali As-Salim",
      "Aqila",
      "Bar Al Ahmadi",
      "Bneidar",
      "Dhaher",
      "Fahaheel",
      "Fahad",
      "Hadiya",
      "Jaber Al-Ali",
      "Jawaher Al Wafra",
      "Jilei'a",
      "Khairan",
      "Mahbula",
      "Mangaf",
      "Miqwa",
      "New Khairan City",
      "New Wafra",
      "Nuwaiseeb",
      'Riqqa',
      "Sabah Al-Ahmad City",
      "Sabah Al-Ahmad Nautical City",
      "Sabahiya",
      "Shu'aiba (North)",
      "Shu'aiba (South)",
      "South Sabahiya",
      'Wafra',
      "Zoor",
      "Zuhar",

    ]
  },
  {
    gov: 'Farwaniya', area: [
      "Abdullah Al-Mubarak",
      'Airport District',
      "Andalous",
      "Ardiya",
      "Ardiya Herafiya",
      "Ardiya Industrial Area",
      "Ashbelya",
      "Dhajeej",
      "Farwaniya",
      "Fordous",
      "Jleeb Al-Shuyoukh",
      "Khaitan",
      'Omariya',
      "Rabiya",
      "Rai",
      "Al-Riggae",
      "Rihab",
      "Sabah Al-Nasser",
      "Sabaq Al Hajan"


    ]
  },
  {
    gov: 'Jahra', area: [
      "Abdali",
      "Al Nahda / East Sulaibikhat",
      "Amghara",
      "Bar Jahra",
      "Jahra",
      "Jahra Industrial Area",
      "Kabad",
      "Naeem",
      "Nasseem",
      "Oyoun",
      "Qasr",
      "Saad Al Abdullah City",
      "Salmi",
      "Sikrab",
      'South Doha / Qairawān',
      "Subiya",
      'Sulaibiya',
      "Sulaibiya Agricultural Area",
      'Taima',
      'Waha'


    ]
  }

  ]
  constructor(private route: ActivatedRoute) { }
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  dropdownSettings1 = {}
  ngOnInit(): void {
    this.dtOptions = {
      pagingType: "full_numbers",
      pageLength: 5,
      destroy: true,
      order: [],
      info: false,
      lengthChange: false,
      select: true,
      columnDefs: [{ orderable: false, targets: [] }],
      language: {
        zeroRecords: "No Data to display",
        paginate: {
          next: ">>", // or '→'
          previous: "<<", // or '←',
          first: null,
          last: null,
        },
      },
      dom: "Bfrtip",
      buttons: ["print", "excel"],
    };

    this.orderForm = new FormGroup({
      customerName: new FormControl("",),
      bankName: new FormControl(''),
      password: new FormControl(''),
      phoneNumber: new FormControl(''),
      email: new FormControl(''),

      Street: new FormControl(''),
      Governorate: new FormControl(''),
      Area: new FormControl(''),
      Block: new FormControl(''),
      BuildingNo: new FormControl(''),
      floorNo: new FormControl(''),
      status: new FormControl(''),



    });

    this.customerForm = new FormGroup({
      customerName: new FormControl(''),
      customerBranches: new FormControl(''),
      customerReceiveDate: new FormControl(''),



    });
    this.orderDetails = [
      {
        customerName: 'Driver  1', password: 9889231821, phoneNumber: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 1, email: "dummy@gmail.com", status: 'active'
        , bankName: 'Test Bank'
      },
      {
        bankName: 'Test Bank',
        customerName: 'Driver   2', password: 9889231821, phoneNumber: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 2, email: "dummy@gmail.com", status: 'active'
      },
      {
        bankName: 'Test Bank',
        customerName: 'Driver 3', password: 9889231821, phoneNumber: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 3, email: "dummy@gmail.com", status: 'active'
      },
      {
        bankName: 'Test Bank',
        customerName: 'Driver 4', password: 9889231821, phoneNumber: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 4, email: "dummy@gmail.com", status: 'active'
      },
      {
        bankName: 'Test Bank',
        customerName: 'Driver 5', password: 9889231821, phoneNumber: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 5, email: "dummy@gmail.com", status: 'active'
      },
      {
        bankName: 'Test Bank',
        customerName: 'Driver 6', password: 9889231821, phoneNumber: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 6, email: "dummy@gmail.com", status: 'active'
      },

    ]
    //this.rerender()
    this.dropdownList = [
      { item_id: 1, item_text: 'Capital' },
      { item_id: 2, item_text: 'Hawalli' },
      { item_id: 3, item_text: 'Mubarak Al-Kaber' },
      { item_id: 4, item_text: 'Ahmadi' },
      { item_id: 5, item_text: 'Farwaniya' },
      { item_id: 5, item_text: 'Jahra' }

    ];
    this.selectedItems = [
      { item_id: 3, item_text: 'Pune' },
      { item_id: 4, item_text: 'Navsari' }
    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.dropdownSettings1 = {
      singleSelection: false,


      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };


    this.route.params.subscribe(params => {
      if (params && params.id != 0) {
        this.totalOrder = params.id
      }
    })
  }
  addNewOrder() {
    this.areaList = []
    this.addOrEdit = 'Add'
    this.orderForm.reset()
    this.isEdit = false;
    // this.locationDetails.push(this.locationForm.value)
  }
  selectedItem(id) {
    this.areaList = []
    this.addOrEdit = 'Edit'
    this.isEdit = true;
    this.selectedLocationId = id
    console.log('id: ', id);
    let selectedLocation = this.orderDetails.filter(arr => {
      return arr.id == id
    })
    console.log('selectedLocation: ', selectedLocation);
    if (selectedLocation && selectedLocation.length > 0) {

      this.orderForm = new FormGroup({



        customerName: new FormControl(selectedLocation[0]['customerName']),
        bankName: new FormControl(selectedLocation[0]['bankName']),
        password: new FormControl(selectedLocation[0]['password']),
        phoneNumber: new FormControl(selectedLocation[0]['phoneNumber']),
        email: new FormControl(selectedLocation[0]['email']),

        Street: new FormControl(selectedLocation[0]['Street']),
        Governorate: new FormControl(selectedLocation[0]['Governorate']),
        Area: new FormControl(selectedLocation[0]['Area']),
        Block: new FormControl(selectedLocation[0]['Block']),
        BuildingNo: new FormControl(selectedLocation[0]['BuildingNo']),
        floorNo: new FormControl(selectedLocation[0]['floorNo']),

        status: new FormControl(selectedLocation[0]['status']),



      });
    }

  }
  onClick() {
    if (this.isEdit) {
      this.orderForm.value['id'] = this.selectedLocationId;
      this.orderDetails.forEach((data, index) => {
        if (data.id == this.selectedLocationId) {
          this.orderDetails[index] = this.orderForm.value
          console.log('data: ', this.orderForm.value);
        }
      })
    }
    if (!this.isEdit) {
      this.orderDetails.push(this.orderForm.value)
      console.log(this.orderForm.value)
    }


  }
  onSaveCompanyDetails() {
    // this.customerForm.value
    console.log('   this.customerForm.value: ', this.customerForm.value);

  }
  changeGovernorate(value) {
    console.log('value: ', value);
    console.log('this.areaListByGovernorate: ', this.areaListByGovernorate);
    let selectGov = this.areaListByGovernorate.filter(arr => {
      return arr.gov == value
    })
    console.log('selectGov: ', selectGov);
    if (selectGov && selectGov.length > 0) {
      this.areaList = selectGov[0]['area']
    }

  }

  deleteOrder(id) {
    this.idForDelete = id;
  }
  confirmDelete() {
    this.orderDetails.splice(this.idForDelete, 1)
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    }
    );

  }

  onItemSelect(item: any) {
    console.log(item);

    //  console.log('value: ', );
    console.log('this.areaListByGovernorate: ', this.areaListByGovernorate);
    let selectGov = this.areaListByGovernorate.filter(arr => {
      return arr.gov == item.item_text;
    })
    console.log('selectGov: ', selectGov);
    if (selectGov && selectGov.length > 0) {
      this.areaList.push(selectGov[0]['area'])
      console.log(' this.areaList: ', this.areaList);
    }
  }
  onSelectAll(items: any) {
    this.areaList = this.areaListByGovernorate['area']
    console.log(' this.areaList: ', this.areaList);
    console.log(items);
  }
  telInputObject(value) {
    console.log(value, "value");
  }
}
