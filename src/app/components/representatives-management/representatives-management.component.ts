import { AfterViewInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { MouseEvent } from '@agm/core';

import { Validators } from '@angular/forms';
import { OnDestroy } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';


@Component({
  selector: 'app-representatives-management',
  templateUrl: './representatives-management.component.html',
  styleUrls: ['./representatives-management.component.css']
})
export class RepresentativesManagementComponent implements OnInit, AfterViewInit, OnDestroy {
  dtElement: DataTableDirective;

  dtOptions: any = {};
  idForDelete: any;
  dtTrigger: Subject<any> = new Subject();

  url: any = "./assets/img/faces/avtar.jpeg"
  orderDetails = []
  orderForm;
  isEdit: boolean;
  selectedLocationId: any;
  addOrEdit;
  dataSource;
  customerForm;
  areaList: string[] = []
  govList = []
  checkList = []
  isActiveClass = "trucking"
  governorateList = [
    'Capital',
    'Hawalli',
    'Mubarak Al-Kaber',
    'Ahmadi',
    'Farwaniya',
    'Jahra'
  ]
  areaListByGovernorate = [{
    gov: 'Capital', area: [
      "Abdulla Al-Salem",
      "Adailiya",
      "Bnaid Al-Qar",
      "Daʿiya",
      "Dasma",
      "Doha",
      "Doha Port",
      "Faiha",
      "Failaka",
      "Ghornata",
      "Jaber Al-Ahmad City",
      "Jibla",
      "Kaifan",
      "Khaldiya",
      "Mansūriya",
      "Mirgab",
      "Nahdha",
      "North West Sulaibikhat",
      "Nuzha",
      "Qadsiya",
      "Qurtuba",
      "Rawda",
      "Shamiya",
      "Sharq",
      "Shuwaikh",
      "Shuwaikh Industrial Area",
      "Shuwaikh Port",
      "Sulaibikhat",
      "Surra",
      "Umm an Namil Island",
      "Yarmouk",

    ]
  }
    ,
  {
    gov: 'Hawalli', area: [
      "Anjafa",
      "Bayān",
      "Bi'da",
      "Hawally",
      "Hittin",
      "Jabriya",
      "Maidan Hawalli",
      "Mishrif",
      "Mubarak Al-Jabir",
      "Nigra",
      'Rumaithiya',
      "Salam",
      "Salmiya",
      "Salwa",
      "Sha'ab",
      "Shuhada",
      "Siddiq",
      "South Surra",
      'Zahra'


    ]
  },
  {
    gov: 'Mubarak Al-Kaber', area: [
      " Abu Al Hasaniya",
      "Abu Futaira",
      "Adān",
      "Al Qurain",
      "Al-Qusour",
      "Fintās",
      "Funaitīs",
      "Misīla",
      "Mubarak Al-Kabeer",
      "Sabah Al-Salem",
      "Sabhān",
      "South Wista",
      "Wista",


    ]
  },
  {
    gov: 'Ahmadi', area: [
      "Abu Halifa",
      "Abdullah Port",
      "Ahmadi",
      "Ali As-Salim",
      "Aqila",
      "Bar Al Ahmadi",
      "Bneidar",
      "Dhaher",
      "Fahaheel",
      "Fahad",
      "Hadiya",
      "Jaber Al-Ali",
      "Jawaher Al Wafra",
      "Jilei'a",
      "Khairan",
      "Mahbula",
      "Mangaf",
      "Miqwa",
      "New Khairan City",
      "New Wafra",
      "Nuwaiseeb",
      'Riqqa',
      "Sabah Al-Ahmad City",
      "Sabah Al-Ahmad Nautical City",
      "Sabahiya",
      "Shu'aiba (North)",
      "Shu'aiba (South)",
      "South Sabahiya",
      'Wafra',
      "Zoor",
      "Zuhar",

    ]
  },
  {
    gov: 'Farwaniya', area: [
      "Abdullah Al-Mubarak",
      'Airport District',
      "Andalous",
      "Ardiya",
      "Ardiya Herafiya",
      "Ardiya Industrial Area",
      "Ashbelya",
      "Dhajeej",
      "Farwaniya",
      "Fordous",
      "Jleeb Al-Shuyoukh",
      "Khaitan",
      'Omariya',
      "Rabiya",
      "Rai",
      "Al-Riggae",
      "Rihab",
      "Sabah Al-Nasser",
      "Sabaq Al Hajan"


    ]
  },
  {
    gov: 'Jahra', area: [
      "Abdali",
      "Al Nahda / East Sulaibikhat",
      "Amghara",
      "Bar Jahra",
      "Jahra",
      "Jahra Industrial Area",
      "Kabad",
      "Naeem",
      "Nasseem",
      "Oyoun",
      "Qasr",
      "Saad Al Abdullah City",
      "Salmi",
      "Sikrab",
      'South Doha / Qairawān',
      "Subiya",
      'Sulaibiya',
      "Sulaibiya Agricultural Area",
      'Taima',
      'Waha'


    ]
  }

  ]
  checkedCondition: boolean;
  constructor() {
    $(document).ready(function () {
      $(".upload-button").on("click", function () {
        $(".file-upload").click();
      });
    });

  }
  selectedArea = []
  dropdownList = [];
  selectedItems = [];
  dropdownListStatus = [];
  dropdownSettings = {};
  dropdownSettings1 = {}
  passwordType = 'password'
  ngOnInit(): void {
    this.dtOptions = {
      pagingType: "full_numbers",
      pageLength: 5,
      destroy: true,
      order: [],
      info: false,
      lengthChange: false,
      select: true,
      columnDefs: [{ orderable: false, targets: [] }],
      language: {
        zeroRecords: "No Data to display",
        paginate: {
          next: ">>", // or '→'
          previous: "<<", // or '←',
          first: null,
          last: null,
        },
      },
      dom: "Bfrtip",
      buttons: ["print", "excel"],
    };

    this.orderForm = new FormGroup({
      customerName: new FormControl("",),
      bankName: new FormControl(''),
      password: new FormControl(''),
      phoneNumber: new FormControl(''),
      email: new FormControl(''),

      Street: new FormControl(''),
      Governorate: new FormControl(''),
      Area: new FormControl(''),
      Block: new FormControl(''),
      BuildingNo: new FormControl(''),
      floorNo: new FormControl(''),
      status: new FormControl(''),

      firstName: new FormControl(''),
      lastName: new FormControl(''),
      dl: new FormControl(''),
      userName: new FormControl(''),
      role: new FormControl(''),
      id: new FormControl(''),
      Section: new FormControl(''),

      LicencePlate: new FormControl(''),
      VehicleColor: new FormControl(''),
      vDate: new FormControl(''),



    });

    this.customerForm = new FormGroup({
      customerName: new FormControl(''),
      customerBranches: new FormControl(''),
      customerReceiveDate: new FormControl(''),



    });
    this.orderDetails = [



    ]
    //this.rerender()
    this.dropdownList = [

      { item_id: 2, item_text: 'Local Post' },
      { item_id: 3, item_text: 'Int. Post' },
      { item_id: 4, item_text: 'Delivery Orders' },


    ];

    this.dropdownListStatus = [

      { item_id: 2, item_text: 'Active' },
      { item_id: 3, item_text: 'InActive' },
      { item_id: 4, item_text: 'Blocked' },


    ];
    this.selectedItems = [
      { item_id: 3, item_text: 'Pune' },
      { item_id: 4, item_text: 'Navsari' }
    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.dropdownSettings1 = {
      singleSelection: false,


      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }
  changePasswordType() {
    if (this.passwordType == 'password') {
      this.passwordType = 'text'
    }
    else {
      this.passwordType = 'password'
    }
  }
  addNewOrder() {
    this.areaList = []
    this.addOrEdit = 'Add'
    this.orderForm.reset()
    this.isEdit = false;
    // this.locationDetails.push(this.locationForm.value)
  }
  selectedItem(id) {
    this.areaList = []
    this.addOrEdit = 'Edit'
    this.isEdit = true;
    this.selectedLocationId = id

    let selectedLocation = this.orderDetails.filter(arr => {
      return arr.id == id
    })

    if (selectedLocation && selectedLocation.length > 0) {

      // this.orderForm = new FormGroup({



      //   customerName: new FormControl(selectedLocation[0]['customerName']),
      //   bankName: new FormControl(selectedLocation[0]['bankName']),
      //   password: new FormControl(selectedLocation[0]['password']),
      //   phoneNumber: new FormControl(selectedLocation[0]['phoneNumber']),
      //   email: new FormControl(selectedLocation[0]['email']),

      //   Street: new FormControl(selectedLocation[0]['Street']),
      //   Governorate: new FormControl(selectedLocation[0]['Governorate']),
      //   Area: new FormControl(selectedLocation[0]['Area']),
      //   Block: new FormControl(selectedLocation[0]['Block']),
      //   BuildingNo: new FormControl(selectedLocation[0]['BuildingNo']),
      //   floorNo: new FormControl(selectedLocation[0]['floorNo']),

      //   status: new FormControl(selectedLocation[0]['status']),



      // });
    }

  }
  onClick() {
    if (this.isEdit) {
      this.orderForm.value['id'] = this.selectedLocationId;
      this.orderDetails.forEach((data, index) => {
        if (data.id == this.selectedLocationId) {
          this.orderDetails[index] = this.orderForm.value

        }
      })
    }
    if (!this.isEdit) {
      this.orderDetails.push(this.orderForm.value)

    }


  }
  onSaveCompanyDetails() {
    // this.customerForm.value


  }
  changeGovernorate(value) {


    let selectGov = this.areaListByGovernorate.filter(arr => {
      return arr.gov == value
    })

    if (selectGov && selectGov.length > 0) {
      this.areaList = selectGov[0]['area']
    }

  }

  deleteOrder(id) {
    this.idForDelete = id;
  }
  confirmDelete() {
    this.orderDetails.splice(this.idForDelete, 1)
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    }
    );

  }

  onSelectAll(items: any) {
    this.areaList = this.areaListByGovernorate['area']


  }
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      }
    }
  }
  sendData(value) {


    this.checkList.push(value)

  }
  sendDataGovernorate(value) {


    console.log('value: ', value, this.govList, this.checkList);
    let checkData = false
    for (let i = 0; i < this.govList.length; i++) {
      if (this.govList[i] == value) {
        this.govList.splice(i, 1);
        checkData = true
        break;
      }
    }
    if (checkData) {
      this.areaListByGovernorate.forEach(arr => {
        let checkCheckList = false
        if (arr.gov == value) {
          arr.area.forEach((data, index) => {
            for (let j = 0; j < this.checkList.length; j++) {
              //  console.log('this.checkList[j] == data: ', this.checkList[j], data);
              if (this.checkList[j] == data) {
                this.checkList.splice(j, 1);
                break;
              }
            }

            // if (this.checkList.indexOf(data) > 0) {
            //   this.checkList.splice(index, 1)
            // }

          })
        }
        // this.rerender()
      })
    }
    else {
      this.govList.push(value)
      this.areaListByGovernorate.forEach(arr => {
        let checkCheckList = false
        if (arr.gov == value) {
          arr.area.forEach(data => {

            this.checkList.push(data)
          })
        }
        // this.rerender()
      })
    }




  }
  checkedArea(value) {
    //  console.log('value: ', value);
    //  let check = true
    for (let j = 0; j < this.checkList.length; j++) {
      // console.log('this.checkList[j] == data: ', this.checkList[j], data);
      if (this.checkList[j] == value) {
        //  check = false
        return true

      }
    }
    return false
    //  let checked = this.checkList.indexOf(value) > 0 ? true : false

  }





  sendDataGov(value) {

    console.log('value: ', value, this.govList, this.checkList);
    let checkData = false
    for (let i = 0; i < this.govList.length; i++) {
      if (this.govList[i] == value) {
        this.govList.splice(i, 1);
        checkData = true
        break;
      }
    }
    if (checkData) {
      this.areaListByGovernorate.forEach(arr => {
        let checkCheckList = false
        if (arr.gov == value) {
          arr.area.forEach((data, index) => {
            for (let j = 0; j < this.checkList.length; j++) {
              //  console.log('this.checkList[j] == data: ', this.checkList[j], data);
              if (this.checkList[j] == data) {
                this.checkList.splice(j, 1);
                break;
              }
            }

            // if (this.checkList.indexOf(data) > 0) {
            //   this.checkList.splice(index, 1)
            // }

          })
        }
        // this.rerender()
      })
    }
    else {
      this.govList.push(value)
      this.areaListByGovernorate.forEach(arr => {
        let checkCheckList = false
        if (arr.gov == value) {
          arr.area.forEach(data => {

            this.checkList.push(data)
          })
        }
        // this.rerender()
      })
    }



  }
  selectAll(value) {

    this.checkList.push(value)
    let selectGov = this.areaListByGovernorate.forEach(arr => {

      if (arr.gov == value) {
        arr.area.forEach(data => {
          this.checkList.push(data)
        })
      }
      // this.rerender()
    })

  }
  unselectAll(value) {
    let selectGov = this.areaListByGovernorate.forEach(arr => {

      if (arr.gov == value) {
        arr.area.forEach((data, index) => {
          if (this.checkList.indexOf(data) > 0) {
            this.checkList.splice(index, 1)
          }
        })
      }
      // this.rerender()
    })

  }
  checked(value) {
    console.log('value: ', value, this.selectedArea);
    if (this.checkedCondition) {
      return true
    }
    //  console.log('value: ', value, this.selectedArea);
    //  console.log('this.selectedArea.length: ', this.selectedArea.length);
    for (let i = 0; i < this.selectedArea.length; i++) {
      if (this.selectedArea[i] == value) {
        console.log('this.selectedArea[i] == value: ', this.selectedArea[i], value);
        return true

      }
    }
    return false;
  }
  checkedSelected(value) {
    this.checkedCondition = false;
    // let checkData = false
    // for (let i = 0; i < this.selectedArea.length; i++) {
    //   if (this.selectedArea[i] == value) {
    //     this.selectedArea.splice(i, 1);
    //     checkData = true
    //     break;
    //   }
    // }

    // if (!checkData) {

    //   this.selectedArea.push(value)
    // }


  }
  checkedAll(value) {
    console.log('value: ', value);
    // console.log('value: ', value, this.selectedArea, this.checkList);
    if (value) {
      this.checkedCondition = true;
      this.selectedArea = this.checkList
      console.log('  this.selectedArea: ', this.selectedArea);

    }
    else {
      this.checkedCondition = false;
      this.selectedArea = []
    }

  }
  changeBorder(value) {
    this.isActiveClass = value
  }
}

