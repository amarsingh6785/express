import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepresentativesManagementComponent } from './representatives-management.component';

describe('RepresentativesManagementComponent', () => {
  let component: RepresentativesManagementComponent;
  let fixture: ComponentFixture<RepresentativesManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepresentativesManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RepresentativesManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
