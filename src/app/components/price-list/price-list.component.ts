import { AfterViewInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { MouseEvent } from '@agm/core';

import { Validators } from '@angular/forms';
import { OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-price-list',
  templateUrl: './price-list.component.html',
  styleUrls: ['./price-list.component.css']
})
export class PriceListComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  newlocationDetails;
  unsubscribedata;
  dtOptions: any = {};
  idForDelete: any;
  areaList = []
  dtTrigger: Subject<any> = new Subject();
  checkAll = false
  locationDetails = []
  locationForm;
  isEdit: boolean;
  selectedLocationId: any;
  addOrEdit;
  dataSource;
  companyForm;
  masterCheck = false
  countryList;
  governorateList = [
    'Capital',
    'Hawalli',
    'Mubarak Al-Kaber',
    'Ahmadi',
    'Farwaniya',
    'Jahra'
  ]
  areaListByGovernorate = [{
    gov: 'Capital', area: [
      "Abdulla Al-Salem",
      "Adailiya",
      "Bnaid Al-Qar",
      "Daʿiya",
      "Dasma",
      "Doha",
      "Doha Port",
      "Faiha",
      "Failaka",
      "Ghornata",
      "Jaber Al-Ahmad City",
      "Jibla",
      "Kaifan",
      "Khaldiya",
      "Mansūriya",
      "Mirgab",
      "Nahdha",
      "North West Sulaibikhat",
      "Nuzha",
      "Qadsiya",
      "Qurtuba",
      "Rawda",
      "Shamiya",
      "Sharq",
      "Shuwaikh",
      "Shuwaikh Industrial Area",
      "Shuwaikh Port",
      "Sulaibikhat",
      "Surra",
      "Umm an Namil Island",
      "Yarmouk",

    ]
  }
    ,
  {
    gov: 'Hawalli', area: [
      "Anjafa",
      "Bayān",
      "Bi'da",
      "Hawally",
      "Hittin",
      "Jabriya",
      "Maidan Hawalli",
      "Mishrif",
      "Mubarak Al-Jabir",
      "Nigra",
      'Rumaithiya',
      "Salam",
      "Salmiya",
      "Salwa",
      "Sha'ab",
      "Shuhada",
      "Siddiq",
      "South Surra",
      'Zahra'


    ]
  },
  {
    gov: 'Mubarak Al-Kaber', area: [
      " Abu Al Hasaniya",
      "Abu Futaira",
      "Adān",
      "Al Qurain",
      "Al-Qusour",
      "Fintās",
      "Funaitīs",
      "Misīla",
      "Mubarak Al-Kabeer",
      "Sabah Al-Salem",
      "Sabhān",
      "South Wista",
      "Wista",


    ]
  },
  {
    gov: 'Ahmadi', area: [
      "Abu Halifa",
      "Abdullah Port",
      "Ahmadi",
      "Ali As-Salim",
      "Aqila",
      "Bar Al Ahmadi",
      "Bneidar",
      "Dhaher",
      "Fahaheel",
      "Fahad",
      "Hadiya",
      "Jaber Al-Ali",
      "Jawaher Al Wafra",
      "Jilei'a",
      "Khairan",
      "Mahbula",
      "Mangaf",
      "Miqwa",
      "New Khairan City",
      "New Wafra",
      "Nuwaiseeb",
      'Riqqa',
      "Sabah Al-Ahmad City",
      "Sabah Al-Ahmad Nautical City",
      "Sabahiya",
      "Shu'aiba (North)",
      "Shu'aiba (South)",
      "South Sabahiya",
      'Wafra',
      "Zoor",
      "Zuhar",

    ]
  },
  {
    gov: 'Farwaniya', area: [
      "Abdullah Al-Mubarak",
      'Airport District',
      "Andalous",
      "Ardiya",
      "Ardiya Herafiya",
      "Ardiya Industrial Area",
      "Ashbelya",
      "Dhajeej",
      "Farwaniya",
      "Fordous",
      "Jleeb Al-Shuyoukh",
      "Khaitan",
      'Omariya',
      "Rabiya",
      "Rai",
      "Al-Riggae",
      "Rihab",
      "Sabah Al-Nasser",
      "Sabaq Al Hajan"


    ]
  },
  {
    gov: 'Jahra', area: [
      "Abdali",
      "Al Nahda / East Sulaibikhat",
      "Amghara",
      "Bar Jahra",
      "Jahra",
      "Jahra Industrial Area",
      "Kabad",
      "Naeem",
      "Nasseem",
      "Oyoun",
      "Qasr",
      "Saad Al Abdullah City",
      "Salmi",
      "Sikrab",
      'South Doha / Qairawān',
      "Subiya",
      'Sulaibiya',
      "Sulaibiya Agricultural Area",
      'Taima',
      'Waha'


    ]
  }

  ]
  zoom: number = 8;
  lat: number = 51.673858;
  lng: number = 7.815982;
  checkList: any = {};
  sundayCheck;
  saturdayCheck;
  mondayCheck;
  tuesdayCheck;
  wednesdayCheck;
  thursdayCheck;
  fridayCheck;
  zoneList = []
  dropdownListStatus = [

    { item_id: 2, item_text: 'Branch 1' },
    { item_id: 3, item_text: 'Branch 2' },
    { item_id: 4, item_text: 'Branch 3' },


  ];
  dropdownSettings = {
    singleSelection: false,
    idField: 'item_id',
    textField: 'item_text',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
  constructor(private http: HttpClient) {
    // $(document).ready(function () {
    //   (<any>$('.timepicker')).timepicker({
    //     timeFormat: 'HH:mm',
    //     interval: 60,
    //     defaultTime: '10',
    //   });
    // });
  }

  ngOnInit(): void {
    // this.http.get("./assets/json/country.json").subscribe(data => {
    //   console.log(data);
    //   this.countryList = data

    // })
    this.countryList = [
      'IP',
      'IE',
      'IPF',
      'IEE',
    ]

    this.zoneList = [
      "zone 1",
      "zone 2",
      "zone 3",
      "zone 4",
      "zone 5",
      "zone 6",
    ]

    this.dtOptions = {
      pagingType: "full_numbers",
      pageLength: 5,
      destroy: true,
      order: [],
      info: false,
      lengthChange: false,
      select: true,
      columnDefs: [{ orderable: false, targets: [] }],
      language: {
        zeroRecords: "No Data to display",
        paginate: {
          next: ">>", // or '→'
          previous: "<<", // or '←',
          first: null,
          last: null,
        },
      },
      dom: "Bfrtip",
      buttons: [],
    };
    this.getLocation()

    this.locationForm = new FormGroup({
      serviceType: new FormControl(),
      zones: new FormControl(),
      weight: new FormControl(),
      price: new FormControl(),

    });

    this.companyForm = new FormGroup({
      companyName: new FormControl(''),
      companyEmail: new FormControl(''),
      companyOtherEmail: new FormControl(''),


      companyPhone: new FormControl(),
      companyWebsite: new FormControl(''),


    });




  }
  getLocation() {
    // this.rerender()

    this.locationDetails = []


    this.locationDetails = [
      { serviceType: 'IE', zones: 'A', weight: 1, id: 1, price: 5000.12 },
      { serviceType: 'IE', zones: 'A', id: 2, weight: 1.3, price: 2.1200 },
      { serviceType: 'IE', zones: 'A', id: 3, weight: 1.5, price: 3.2200 },
      { serviceType: 'IE', zones: 'A', id: 4, weight: .5, price: 5.2220 },
      { serviceType: 'IE', zones: 'A', id: 5, weight: 2, price: 1.220 },
    ]
    //  this.rerender()



  }
  addNewLocation() {
    this.areaList
    this.addOrEdit = 'Add'
    this.locationForm.reset()
    this.isEdit = false;
    // this.locationDetails.push(this.locationForm.value)
  }
  selectedItem(id) {
    this.areaList = []
    this.addOrEdit = 'Edit'
    this.isEdit = true;
    this.selectedLocationId = id
    console.log('id: ', id);
    let selectedLocation = this.locationDetails.filter(arr => {
      return arr.id == id
    })
    console.log('selectedLocation: ', selectedLocation);
    if (selectedLocation && selectedLocation.length > 0) {

      this.locationForm = new FormGroup({
        serviceType: new FormControl(selectedLocation[0]['serviceType']),
        zones: new FormControl(selectedLocation[0]['zones']),
        weight: new FormControl(selectedLocation[0]['weight']),
        price: new FormControl(selectedLocation[0]['price']),


      });
    }

  }
  onClick() {

    if (this.isEdit) {
      this.locationForm.value['id'] = this.selectedLocationId;
      this.locationDetails.forEach((data, index) => {
        if (data.id == this.selectedLocationId) {
          this.locationDetails[index] = this.locationForm.value
          console.log('data: ', this.locationForm.value);
        }
      })
    }
    if (!this.isEdit) {
      console.log('this.locationForm.value: ', this.locationForm.value);
      this.locationDetails.push(this.locationForm.value)
      console.log(this.locationForm.value)
    }


  }
  onSaveCompanyDetails() {
    // this.companyForm.value
    console.log('   this.companyForm.value: ', this.companyForm.value);
    this.companyForm = new FormGroup({
      companyName: new FormControl(this.companyForm.value['companyName']),
      companyEmail: new FormControl(this.companyForm.value['companyEmail']),
      companyOtherEmail: new FormControl(this.companyForm.value['companyOtherEmail']),


      companyPhone: new FormControl(this.companyForm.value['companyPhone']),
      companyWebsite: new FormControl(this.companyForm.value['companyWebsite']),


    });
  }

  changeGovernorate(value) {
    let temp
    console.log('value: ', value);
    this.zoneList = [];
    temp = this.countryList.filter(arr => {
      return arr.name == value
    })
    if (temp && temp.length > 0) {
      console.log('temp: ', temp);
      this.zoneList = temp[0]['timezones']
      console.log('this.zoneList: ', this.zoneList);
    }

  }

  deleteLocation(id) {
    this.idForDelete = id;
  }
  confirmDelete() {
    this.locationDetails.splice(this.idForDelete, 1)
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    }
    );
  }
  changeCheckBox(value, day) {
    console.log('value: ', value);
    if (!value) {
      this.masterCheck = value
      console.log(' this.masterCheck: ', this.masterCheck);
    }
    console.log('value, day: ', value, day);
    this.checkList[day] = value
    console.log('value: ', value);

  }
  ischeck() {
    if (this.masterCheck) {
      return true
    }
    return false
  }
  checkedAll(value) {
    console.log('value: ', value);

    this.checkAll = value
    this.checkList = {
      'monday': value,
      'tuesday': value,
      'wednesday': value,
      'thursday': value,
      'friday': value,
      'saturday': value,
      'sunday': value,
    }

  }


  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  mapClicked($event: MouseEvent) {
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  markers: marker[] = [
    {
      lat: 51.673858,
      lng: 7.815982,
      label: 'A',
      draggable: true
    },
    {
      lat: 51.373858,
      lng: 7.215982,
      label: 'B',
      draggable: false
    },
    {
      lat: 51.723858,
      lng: 7.895982,
      label: 'C',
      draggable: true
    }
  ]



}
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
