import { AfterViewInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { MouseEvent } from '@agm/core';

import { Validators } from '@angular/forms';
import { OnDestroy } from '@angular/core';

@Component({
  selector: 'app-bulk-order-pick-up-requests',
  templateUrl: './bulk-order-pick-up-requests.component.html',
  styleUrls: ['./bulk-order-pick-up-requests.component.css']
})
export class BulkOrderPickUpRequestsComponent implements OnInit {

  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  newlocationDetails;
  unsubscribedata;
  dtOptions: any = {};
  idForDelete: any;
  areaList = []
  dtTrigger: Subject<any> = new Subject();

  locationDetails = []
  locationForm;
  isEdit: boolean;
  selectedLocationId: any;
  addOrEdit;
  dataSource;
  companyForm;
  dropdownListStatus = [

    { item_id: 2, item_text: 'Branch 1' },
    { item_id: 3, item_text: 'Branch 2' },
    { item_id: 4, item_text: 'Branch 3' },


  ];
  dropdownSettings = {
    singleSelection: false,
    idField: 'item_id',
    textField: 'item_text',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
  governorateList = [
    'Capital',
    'Hawalli',
    'Mubarak Al-Kaber',
    'Ahmadi',
    'Farwaniya',
    'Jahra'
  ]
  areaListByGovernorate = [{
    gov: 'Capital', area: [
      "Abdulla Al-Salem",
      "Adailiya",
      "Bnaid Al-Qar",
      "Daʿiya",
      "Dasma",
      "Doha",
      "Doha Port",
      "Faiha",
      "Failaka",
      "Ghornata",
      "Jaber Al-Ahmad City",
      "Jibla",
      "Kaifan",
      "Khaldiya",
      "Mansūriya",
      "Mirgab",
      "Nahdha",
      "North West Sulaibikhat",
      "Nuzha",
      "Qadsiya",
      "Qurtuba",
      "Rawda",
      "Shamiya",
      "Sharq",
      "Shuwaikh",
      "Shuwaikh Industrial Area",
      "Shuwaikh Port",
      "Sulaibikhat",
      "Surra",
      "Umm an Namil Island",
      "Yarmouk",

    ]
  }
    ,
  {
    gov: 'Hawalli', area: [
      "Anjafa",
      "Bayān",
      "Bi'da",
      "Hawally",
      "Hittin",
      "Jabriya",
      "Maidan Hawalli",
      "Mishrif",
      "Mubarak Al-Jabir",
      "Nigra",
      'Rumaithiya',
      "Salam",
      "Salmiya",
      "Salwa",
      "Sha'ab",
      "Shuhada",
      "Siddiq",
      "South Surra",
      'Zahra'


    ]
  },
  {
    gov: 'Mubarak Al-Kaber', area: [
      " Abu Al Hasaniya",
      "Abu Futaira",
      "Adān",
      "Al Qurain",
      "Al-Qusour",
      "Fintās",
      "Funaitīs",
      "Misīla",
      "Mubarak Al-Kabeer",
      "Sabah Al-Salem",
      "Sabhān",
      "South Wista",
      "Wista",


    ]
  },
  {
    gov: 'Ahmadi', area: [
      "Abu Halifa",
      "Abdullah Port",
      "Ahmadi",
      "Ali As-Salim",
      "Aqila",
      "Bar Al Ahmadi",
      "Bneidar",
      "Dhaher",
      "Fahaheel",
      "Fahad",
      "Hadiya",
      "Jaber Al-Ali",
      "Jawaher Al Wafra",
      "Jilei'a",
      "Khairan",
      "Mahbula",
      "Mangaf",
      "Miqwa",
      "New Khairan City",
      "New Wafra",
      "Nuwaiseeb",
      'Riqqa',
      "Sabah Al-Ahmad City",
      "Sabah Al-Ahmad Nautical City",
      "Sabahiya",
      "Shu'aiba (North)",
      "Shu'aiba (South)",
      "South Sabahiya",
      'Wafra',
      "Zoor",
      "Zuhar",

    ]
  },
  {
    gov: 'Farwaniya', area: [
      "Abdullah Al-Mubarak",
      'Airport District',
      "Andalous",
      "Ardiya",
      "Ardiya Herafiya",
      "Ardiya Industrial Area",
      "Ashbelya",
      "Dhajeej",
      "Farwaniya",
      "Fordous",
      "Jleeb Al-Shuyoukh",
      "Khaitan",
      'Omariya',
      "Rabiya",
      "Rai",
      "Al-Riggae",
      "Rihab",
      "Sabah Al-Nasser",
      "Sabaq Al Hajan"


    ]
  },
  {
    gov: 'Jahra', area: [
      "Abdali",
      "Al Nahda / East Sulaibikhat",
      "Amghara",
      "Bar Jahra",
      "Jahra",
      "Jahra Industrial Area",
      "Kabad",
      "Naeem",
      "Nasseem",
      "Oyoun",
      "Qasr",
      "Saad Al Abdullah City",
      "Salmi",
      "Sikrab",
      'South Doha / Qairawān',
      "Subiya",
      'Sulaibiya',
      "Sulaibiya Agricultural Area",
      'Taima',
      'Waha'


    ]
  }

  ]
  zoom: number = 8;
  lat: number = 51.673858;
  lng: number = 7.815982;
  checkList: any = {};
  sundayCheck;
  saturdayCheck;
  mondayCheck;
  tuesdayCheck;
  wednesdayCheck;
  thursdayCheck;
  fridayCheck;
  constructor() {
    $(':input[type=number]').on('mousewheel', function (e) {
      e.preventDefault();
    });
  }

  ngOnInit(): void {

    this.dtOptions = {
      pagingType: "full_numbers",
      pageLength: 5,
      destroy: true,
      order: [],
      info: false,
      lengthChange: false,
      select: true,
      columnDefs: [{ orderable: false, targets: [] }],
      language: {
        zeroRecords: "No Data to display",
        paginate: {
          next: ">>", // or '→'
          previous: "<<", // or '←',
          first: null,
          last: null,
        },
      },
      dom: "Bfrtip",
      buttons: ["print", "excel"],
    };
    this.getLocation()

    this.locationForm = new FormGroup({
      locationName: new FormControl('', Validators.required),
      mobile_no: new FormControl(''),
      paci: new FormControl(''),


      Street: new FormControl(''),
      Governorate: new FormControl(''),
      Area: new FormControl(''),
      Block: new FormControl(''),
      BuildingNo: new FormControl(''),
      floorNo: new FormControl(''),
      governorateListFilterCtrl: new FormControl('')
    });

    this.companyForm = new FormGroup({
      companyName: new FormControl(''),
      companyEmail: new FormControl(''),
      companyOtherEmail: new FormControl(''),


      companyPhone: new FormControl(),
      companyWebsite: new FormControl(''),


    });




  }
  getLocation() {
    // this.rerender()

    this.locationDetails = []


    this.locationDetails = [
      {
        locationName: 'test location 1 test location 1 test location 1 location 1 test location 1 test location 1', mobile_no: 9889231821, paci: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 1
      },
      {
        locationName: 'test location 2', mobile_no: 9889241821, paci: '721781277122', Governorate: '2overnorate2', Area: 'test area2',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 2
      },
      {
        locationName: 'test location 3', mobile_no: 9889251821, paci: '721781277122', Governorate: 'Governorate3', Area: 'test area3',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 3
      },
      {
        locationName: 'test location 4', mobile_no: 9889261821, paci: '721781277122', Governorate: 'Governorate4', Area: 'test area4',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 4
      },
      {
        locationName: 'test location 5', mobile_no: 9889271821, paci: '721781277122', Governorate: 'Governorate5', Area: 'test area5',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 5
      },
      {
        locationName: 'test location 1 test location 1 test location 1 location 1 test location 1 test location 1', mobile_no: 9889231821, paci: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 6
      },

    ]
    //  this.rerender()



  }
  addNewLocation() {
    this.areaList
    this.addOrEdit = 'Add'
    this.locationForm.reset()
    this.isEdit = false;
    // this.locationDetails.push(this.locationForm.value)
  }
  selectedItem(id) {
    this.areaList = []
    this.addOrEdit = 'Edit'
    this.isEdit = true;
    this.selectedLocationId = id
    console.log('id: ', id);
    let selectedLocation = this.locationDetails.filter(arr => {
      return arr.id == id
    })
    console.log('selectedLocation: ', selectedLocation);
    if (selectedLocation && selectedLocation.length > 0) {

      this.locationForm = new FormGroup({
        locationName: new FormControl(selectedLocation[0]['locationName']),
        mobile_no: new FormControl(selectedLocation[0]['mobile_no']),
        paci: new FormControl(selectedLocation[0]['paci']),


        Street: new FormControl(selectedLocation[0]['Street']),
        Governorate: new FormControl(selectedLocation[0]['Governorate']),
        Area: new FormControl(selectedLocation[0]['Area']),
        Block: new FormControl(selectedLocation[0]['Block']),
        BuildingNo: new FormControl(selectedLocation[0]['BuildingNo']),
        floorNo: new FormControl(selectedLocation[0]['floorNo']),

      });
    }

  }
  onClick() {

    if (this.isEdit) {
      this.locationForm.value['id'] = this.selectedLocationId;
      this.locationDetails.forEach((data, index) => {
        if (data.id == this.selectedLocationId) {
          this.locationDetails[index] = this.locationForm.value
          console.log('data: ', this.locationForm.value);
        }
      })
    }
    if (!this.isEdit) {
      console.log('this.locationForm.value: ', this.locationForm.value);
      this.locationDetails.push(this.locationForm.value)
      console.log(this.locationForm.value)
    }


  }
  onSaveCompanyDetails() {
    // this.companyForm.value
    console.log('   this.companyForm.value: ', this.companyForm.value);
    this.companyForm = new FormGroup({
      companyName: new FormControl(this.companyForm.value['companyName']),
      companyEmail: new FormControl(this.companyForm.value['companyEmail']),
      companyOtherEmail: new FormControl(this.companyForm.value['companyOtherEmail']),


      companyPhone: new FormControl(this.companyForm.value['companyPhone']),
      companyWebsite: new FormControl(this.companyForm.value['companyWebsite']),


    });
  }

  changeGovernorate(value) {
    console.log('value: ', value);
    console.log('this.areaListByGovernorate: ', this.areaListByGovernorate);
    let selectGov = this.areaListByGovernorate.filter(arr => {
      return arr.gov == value
    })
    console.log('selectGov: ', selectGov);
    if (selectGov && selectGov.length > 0) {
      this.areaList = selectGov[0]['area']
    }

  }

  deleteLocation(id) {
    this.idForDelete = id;
  }
  confirmDelete() {
    this.locationDetails.splice(this.idForDelete, 1)
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    }
    );
  }
  changeCheckBox(value, day) {
    this.checkList[day] = value
    console.log('value: ', value);

  }


  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  mapClicked($event: MouseEvent) {
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  markers: marker[] = [
    {
      lat: 51.673858,
      lng: 7.815982,
      label: 'A',
      draggable: true
    },
    {
      lat: 51.373858,
      lng: 7.215982,
      label: 'B',
      draggable: false
    },
    {
      lat: 51.723858,
      lng: 7.895982,
      label: 'C',
      draggable: true
    }
  ]



}
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
