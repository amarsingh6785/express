import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriveManageComponent } from './drive-manage.component';

describe('DriveManageComponent', () => {
  let component: DriveManageComponent;
  let fixture: ComponentFixture<DriveManageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriveManageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DriveManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
