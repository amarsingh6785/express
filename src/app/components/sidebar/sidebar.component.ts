import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
  { path: '/company', title: 'Company Profile', icon: 'person', class: '' },
  { path: '/ManageDriver', title: 'Manage Drivers ', icon: 'person', class: '' },

  { path: '/representativeManagement', title: 'Manage Staff ', icon: 'person', class: '' },

  { path: '/bulkUploading', title: 'Bulk Upload', icon: 'content_paste', class: '' },
  { path: '/bulkOrder', title: 'Bulk Request', icon: 'library_books', class: '' },
  { path: '/createOrder', title: 'Create Order', icon: 'bubble_chart', class: '' },
  { path: '/manageOrder', title: 'Manage Order', icon: 'analytics', class: '' },
  { path: '/distributor', title: 'Distribution Order', icon: 'library_books', class: '' },

  { path: '/customer', title: 'Manage Customer', icon: 'location_on', class: '' },
  { path: '/assignDriver/:0', title: 'Assign Driver', icon: 'person', class: '' },
  { path: '/zones', title: 'Zones', icon: 'crop_landscape', class: '' },
  { path: '/priceList', title: 'Price List', icon: 'money', class: '' },

  { path: '/services', title: 'Services', icon: 'miscellaneous_services', class: '' },

  { path: '/trackOrder/:0', title: 'Track Order', icon: 'timeline', class: '' },
  { path: '/manageRFQ', title: 'Manage RFQ', icon: 'library_books', class: '' },
  // trackOrder
  // manageRFQ
  // manageOrder
  // { path: '/RFQ', title: 'RFQ', icon: 'notifications', class: '' },
  // { path: '/contracts', title: 'Contracts', icon: 'notifications', class: '' },
  // { path: '/invoices', title: 'Invoices', icon: 'notifications', class: '' },
  // { path: '/reports', title: 'Reports', icon: 'library_books', class: '' },
  // { path: '/master', title: 'Master Table', icon: 'dashboard', class: '' },

  //     { path: 'login', component: LoginComponent },
  // { path: 'bulkUploading', component: BulkUploadingComponent },
  // { path: 'bulkOrder', component: BulkOrderPickUpRequestsComponent },
  // { path: 'createOrder', component: CreateOrderComponent },
  //     { path: 'representativeManagement', component: RepresentativesManagementComponent },

];

export const ROUTES2: RouteInfo[] = [
  // { path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
  // { path: '/company', title: 'Company Profile', icon: 'person', class: '' },
  // { path: '/ManageDriver', title: 'Manage Drivers ', icon: 'person', class: '' },

  // { path: '/representativeManagement', title: 'Manage Staff ', icon: 'person', class: '' },

  // { path: '/bulkUploading', title: 'Bulk Upload', icon: 'content_paste', class: '' },
  // { path: '/bulkOrder', title: 'Bulk Request', icon: 'library_books', class: '' },
  // { path: '/createOrder', title: 'Create Order', icon: 'bubble_chart', class: '' },
  // { path: '/distributor', title: 'Distribution Order', icon: 'library_books', class: '' },
  // { path: '/manage-order', title: 'Maps', icon: 'library_books', class: '' },
  // { path: '/customer', title: 'Manage Customer', icon: 'location_on', class: '' },
  // { path: '/assignDriver', title: 'Assign Driver', icon: 'person', class: '' },
  { path: '/RFQ', title: 'RFQ', icon: 'notifications', class: '' },
  { path: '/contracts', title: 'Contracts', icon: 'notifications', class: '' },
  { path: '/invoices', title: 'Invoices', icon: 'notifications', class: '' },
  { path: '/reports', title: 'Reports', icon: 'library_books', class: '' },
  { path: '/master', title: 'Master Table', icon: 'dashboard', class: '' },
  { path: '/manage-order', title: 'Maps', icon: 'library_books', class: '' },
  //     { path: 'login', component: LoginComponent },
  // { path: 'bulkUploading', component: BulkUploadingComponent },
  // { path: 'bulkOrder', component: BulkOrderPickUpRequestsComponent },
  // { path: 'createOrder', component: CreateOrderComponent },
  //     { path: 'representativeManagement', component: RepresentativesManagementComponent },

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  menuItems2: any[];
  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.menuItems2 = ROUTES2.filter(menuItem2 => menuItem2);
    console.log('menuItems2: ', this.menuItems2);
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
}
