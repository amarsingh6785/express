import { AfterViewInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { MouseEvent } from '@agm/core';

import { Validators } from '@angular/forms';
import { OnDestroy } from '@angular/core';

@Component({
  selector: 'app-distributor',
  templateUrl: './distributor.component.html',
  styleUrls: ['./distributor.component.css']
})
export class DistributorComponent implements OnInit {
  dtElement: DataTableDirective;

  dtOptions: any = {};
  idForDelete: any;
  dtTrigger: Subject<any> = new Subject();
  orderDetails = []
  constructor() {
    this.orderDetails = [
      {
        barcode: 232232, AWB: 9889231821, receiverName: 'Test1 User', senderName: 'Test6 Sender', orderDate: '20/04/2021',
        shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Delivered', clientName: 'Test1 Client',
      },
      {
        barcode: 23232, AWB: 9889231821, receiverName: 'Test2 User', senderName: 'Test5 Sender', orderDate: '20/04/2021',
        shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Delivered', clientName: 'Test2 Client',
      },
      {
        barcode: 243232, AWB: 9889231821, receiverName: 'Test3 User', senderName: 'Test4 Sender', orderDate: '20/04/2021',
        shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Delivered', clientName: 'Test3 Client',
      },
      {
        barcode: 32232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test3 Sender', orderDate: '20/04/2021',
        shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Delivered', clientName: 'Test4 Client',
      },
      {
        barcode: 3232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test2 Sender', orderDate: '20/04/2021',
        shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Delivered', clientName: 'Test5 Client',
      },
      {
        barcode: 72323232, AWB: 9889231821, receiverName: 'Test5 User', senderName: 'Test1 Sender', orderDate: '20/04/2021',
        shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Delivered', clientName: 'Test6 Client',
      },

    ]
  }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: "full_numbers",
      pageLength: 5,
      destroy: true,
      order: [],
      info: false,
      lengthChange: false,
      select: true,
      columnDefs: [{ orderable: false, targets: [] }],
      language: {
        zeroRecords: "No Data to display",
        paginate: {
          next: ">>", // or '→'
          previous: "<<", // or '←',
          first: null,
          last: null,
        },
      },
      dom: "Bfrtip",
      buttons: ["print", "excel"],
    };

  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    }
    );

  }

}
