import { AfterViewInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { MouseEvent } from '@agm/core';

import { Validators } from '@angular/forms';
import { OnDestroy } from '@angular/core';



@Component({
  selector: 'app-bulk-uploading',
  templateUrl: './bulk-uploading.component.html',
  styleUrls: ['./bulk-uploading.component.css']
})
export class BulkUploadingComponent implements OnInit, AfterViewInit, OnDestroy {
  dtElement: DataTableDirective;

  dtOptions: any = {};
  idForDelete: any;
  dtTrigger: Subject<any> = new Subject();


  orderDetails = []
  orderForm;
  isEdit: boolean;
  selectedLocationId: any;
  addOrEdit;
  dataSource;
  customerForm;
  areaList = []
  governorateList = [
    'Capital',
    'Hawalli',
    'Mubarak Al-Kaber',
    'Ahmadi',
    'Farwaniya',
    'Jahra'
  ]
  areaListByGovernorate = [{
    gov: 'Capital', area: [
      "Abdulla Al-Salem",
      "Adailiya",
      "Bnaid Al-Qar",
      "Daʿiya",
      "Dasma",
      "Doha",
      "Doha Port",
      "Faiha",
      "Failaka",
      "Ghornata",
      "Jaber Al-Ahmad City",
      "Jibla",
      "Kaifan",
      "Khaldiya",
      "Mansūriya",
      "Mirgab",
      "Nahdha",
      "North West Sulaibikhat",
      "Nuzha",
      "Qadsiya",
      "Qurtuba",
      "Rawda",
      "Shamiya",
      "Sharq",
      "Shuwaikh",
      "Shuwaikh Industrial Area",
      "Shuwaikh Port",
      "Sulaibikhat",
      "Surra",
      "Umm an Namil Island",
      "Yarmouk",

    ]
  }
    ,
  {
    gov: 'Hawalli', area: [
      "Anjafa",
      "Bayān",
      "Bi'da",
      "Hawally",
      "Hittin",
      "Jabriya",
      "Maidan Hawalli",
      "Mishrif",
      "Mubarak Al-Jabir",
      "Nigra",
      'Rumaithiya',
      "Salam",
      "Salmiya",
      "Salwa",
      "Sha'ab",
      "Shuhada",
      "Siddiq",
      "South Surra",
      'Zahra'


    ]
  },
  {
    gov: 'Mubarak Al-Kaber', area: [
      " Abu Al Hasaniya",
      "Abu Futaira",
      "Adān",
      "Al Qurain",
      "Al-Qusour",
      "Fintās",
      "Funaitīs",
      "Misīla",
      "Mubarak Al-Kabeer",
      "Sabah Al-Salem",
      "Sabhān",
      "South Wista",
      "Wista",


    ]
  },
  {
    gov: 'Ahmadi', area: [
      "Abu Halifa",
      "Abdullah Port",
      "Ahmadi",
      "Ali As-Salim",
      "Aqila",
      "Bar Al Ahmadi",
      "Bneidar",
      "Dhaher",
      "Fahaheel",
      "Fahad",
      "Hadiya",
      "Jaber Al-Ali",
      "Jawaher Al Wafra",
      "Jilei'a",
      "Khairan",
      "Mahbula",
      "Mangaf",
      "Miqwa",
      "New Khairan City",
      "New Wafra",
      "Nuwaiseeb",
      'Riqqa',
      "Sabah Al-Ahmad City",
      "Sabah Al-Ahmad Nautical City",
      "Sabahiya",
      "Shu'aiba (North)",
      "Shu'aiba (South)",
      "South Sabahiya",
      'Wafra',
      "Zoor",
      "Zuhar",

    ]
  },
  {
    gov: 'Farwaniya', area: [
      "Abdullah Al-Mubarak",
      'Airport District',
      "Andalous",
      "Ardiya",
      "Ardiya Herafiya",
      "Ardiya Industrial Area",
      "Ashbelya",
      "Dhajeej",
      "Farwaniya",
      "Fordous",
      "Jleeb Al-Shuyoukh",
      "Khaitan",
      'Omariya',
      "Rabiya",
      "Rai",
      "Al-Riggae",
      "Rihab",
      "Sabah Al-Nasser",
      "Sabaq Al Hajan"


    ]
  },
  {
    gov: 'Jahra', area: [
      "Abdali",
      "Al Nahda / East Sulaibikhat",
      "Amghara",
      "Bar Jahra",
      "Jahra",
      "Jahra Industrial Area",
      "Kabad",
      "Naeem",
      "Nasseem",
      "Oyoun",
      "Qasr",
      "Saad Al Abdullah City",
      "Salmi",
      "Sikrab",
      'South Doha / Qairawān',
      "Subiya",
      'Sulaibiya',
      "Sulaibiya Agricultural Area",
      'Taima',
      'Waha'


    ]
  }

  ]
  constructor() { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: "full_numbers",
      pageLength: 5,
      destroy: true,
      order: [],
      info: false,
      lengthChange: false,
      select: true,
      columnDefs: [{ orderable: false, targets: [] }],
      language: {
        zeroRecords: "No Data to display",
        paginate: {
          next: ">>", // or '→'
          previous: "<<", // or '←',
          first: null,
          last: null,
        },
      },
      dom: "Bfrtip",
      buttons: ["print", "excel"],
    };

    this.orderForm = new FormGroup({
      name: new FormControl('', Validators.required),
      case: new FormControl(''),
      receiverName: new FormControl(''),


      Street: new FormControl(''),
      Governorate: new FormControl(''),
      Area: new FormControl(''),
      Block: new FormControl(''),
      BuildingNo: new FormControl(''),
      floorNo: new FormControl(''),
      notes: new FormControl(''),
      barCode: new FormControl(''),



    });

    this.customerForm = new FormGroup({
      customerName: new FormControl(''),
      customerBranches: new FormControl(''),
      customerReceiveDate: new FormControl(''),



    });
    this.orderDetails = [
      {
        name: 'test location 1 test ', case: 9889231821, receiverName: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 1, notes: "dummy data", barCode: 6162162
      },
      {
        name: 'test location 1 test ', case: 9889231821, receiverName: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 2, notes: "dummy data", barCode: 6162162
      },
      {
        name: 'test location 1 test ', case: 9889231821, receiverName: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 3, notes: "dummy data", barCode: 6162162
      },
      {
        name: 'test location 1 test ', case: 9889231821, receiverName: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 4, notes: "dummy data", barCode: 6162162
      },
      {
        name: 'test location 1 test ', case: 9889231821, receiverName: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 5, notes: "dummy data", barCode: 6162162
      },
      {
        name: 'test location 1 test ', case: 9889231821, receiverName: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 6, notes: "dummy data", barCode: 6162162
      },

    ]
    //this.rerender()

  }
  addNewOrder() {
    this.areaList = []
    this.addOrEdit = 'Add'
    this.orderForm.reset()
    this.isEdit = false;
    // this.locationDetails.push(this.locationForm.value)
  }
  selectedItem(id) {
    this.areaList = []
    this.addOrEdit = 'Edit'
    this.isEdit = true;
    this.selectedLocationId = id
    console.log('id: ', id);
    let selectedLocation = this.orderDetails.filter(arr => {
      return arr.id == id
    })
    console.log('selectedLocation: ', selectedLocation);
    if (selectedLocation && selectedLocation.length > 0) {

      this.orderForm = new FormGroup({
        name: new FormControl(selectedLocation[0]['name']),
        case: new FormControl(selectedLocation[0]['case']),
        receiverName: new FormControl(selectedLocation[0]['receiverName']),


        Street: new FormControl(selectedLocation[0]['Street']),
        Governorate: new FormControl(selectedLocation[0]['Governorate']),
        Area: new FormControl(selectedLocation[0]['Area']),
        Block: new FormControl(selectedLocation[0]['Block']),
        BuildingNo: new FormControl(selectedLocation[0]['BuildingNo']),
        floorNo: new FormControl(selectedLocation[0]['floorNo']),
        barCode: new FormControl(selectedLocation[0]['barCode']),
        notes: new FormControl(selectedLocation[0]['notes']),



      });
    }

  }
  onClick() {
    if (this.isEdit) {
      this.orderForm.value['id'] = this.selectedLocationId;
      this.orderDetails.forEach((data, index) => {
        if (data.id == this.selectedLocationId) {
          this.orderDetails[index] = this.orderForm.value
          console.log('data: ', this.orderForm.value);
        }
      })
    }
    if (!this.isEdit) {
      this.orderDetails.push(this.orderForm.value)
      console.log(this.orderForm.value)
    }


  }
  onSaveCompanyDetails() {
    // this.customerForm.value
    console.log('   this.customerForm.value: ', this.customerForm.value);

  }
  changeGovernorate(value) {
    console.log('value: ', value);
    console.log('this.areaListByGovernorate: ', this.areaListByGovernorate);
    let selectGov = this.areaListByGovernorate.filter(arr => {
      return arr.gov == value
    })
    console.log('selectGov: ', selectGov);
    if (selectGov && selectGov.length > 0) {
      this.areaList = selectGov[0]['area']
    }

  }

  deleteOrder(id) {
    this.idForDelete = id;
  }
  confirmDelete() {
    this.orderDetails.splice(this.idForDelete, 1)
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    }
    );

  }
}
