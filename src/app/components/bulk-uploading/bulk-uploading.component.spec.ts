import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkUploadingComponent } from './bulk-uploading.component';

describe('BulkUploadingComponent', () => {
  let component: BulkUploadingComponent;
  let fixture: ComponentFixture<BulkUploadingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BulkUploadingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkUploadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
